import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flash_chat/Models/chatUsers.dart';
import 'package:flutter/material.dart';





class ChatProvider extends ChangeNotifier {
  String? message;
  int chatID = 0;
  int get chatIdNumber => chatID;
  final fireStore = FirebaseFirestore.instance;
  List<ChatUsers> chatUsers = [
  ];
  List<ChatUsers> get userList => chatUsers;

  bool isSwitchedOn = false;
  bool get isClone => isSwitchedOn == true;

  void addNewClone(ChatUsers chatUsers) {
    userList.add(chatUsers);
    notifyListeners();
  }

  String? getMessageFromUser(String messageFromUser) {
    message = messageFromUser;
    return message;
  }

  void toggleSwitch(bool isSwitchOn) {
    isSwitchedOn = isSwitchOn == true ? true : false;
    notifyListeners();
  }
}
