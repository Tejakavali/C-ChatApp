import 'package:flash_chat/Models/constants.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:toast/toast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AddNewUserScreen extends StatefulWidget {
  const AddNewUserScreen({Key? key}) : super(key: key);

  @override
  State<AddNewUserScreen> createState() => _AddNewUserScreenState();
}

class _AddNewUserScreenState extends State<AddNewUserScreen> {
  @override
  Widget build(BuildContext context) {
    bool hasInternet;

    final fireStore = FirebaseFirestore.instance;
    ToastContext().init(context);
    final TextEditingController cloneNameController = TextEditingController();

    Future<bool> checkExist(String docID) async {
      bool exist = false;

      await fireStore
          .collection('clones')
          .doc(docID)
          .get()
          .then((value) => exist = value.exists);
      return exist;
    }

    return Scaffold(
      body: Container(
        color: const Color(0xff757575),
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          child: Column(
            children: [
              addVerticalSpacing(10),
              const Text(
                'Create a new clone',
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              addVerticalSpacing(20),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: TextField(
                  controller: cloneNameController,
                  cursorColor: Colors.black,
                  minLines: 1,
                  maxLines: 1,
                  keyboardType: TextInputType.name,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: const Color(0xff757575),
                    focusColor: Colors.white,
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Colors.transparent,
                          width: 0,
                          style: BorderStyle.none),
                      borderRadius: BorderRadius.circular(25),
                    ),
                    border: OutlineInputBorder(
                      borderSide:
                          const BorderSide(width: 0, style: BorderStyle.none),
                      borderRadius: BorderRadius.circular(25),
                    ),
                    hintText: "Enter your clones' name",
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 10.0, horizontal: 20.0),
                  ),
                ),
              ),
              addVerticalSpacing(10),
              SizedBox(
                width: 150,
                height: 50,
                child: ProgressButton(
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                    strokeWidth: 2,
                    child: const Text(
                      "Create clone",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                      ),
                    ),
                    onPressed: (AnimationController controller) async {
                      bool cloneExists =
                          await checkExist(cloneNameController.text.trim());
                      hasInternet =
                          await InternetConnectionChecker().hasConnection;
                      if (cloneExists) {
                        Toast.show("Clone already exists",
                            backgroundColor: Colors.amberAccent.shade700,
                            duration: Toast.lengthShort, gravity: Toast.center);
                        controller.reverse();
                      } else if (cloneNameController.text.trim().isEmpty) {
                        Toast.show("Type in a valid name",
                            duration: Toast.lengthShort,
                            gravity: Toast.center,
                            backgroundColor: Colors.amberAccent);
                      } else if (hasInternet == false) {
                        Toast.show("No internet connection",
                            duration: Toast.lengthShort,
                            gravity: Toast.center,
                            backgroundColor: Colors.amberAccent);
                      } else {
                        if (controller.isCompleted) {
                          controller.reverse();
                        } else {
                          controller.forward();
                          await fireStore
                              .collection('clones')
                              .doc(cloneNameController.text)
                              .set({
                            'cloneName': cloneNameController.text,
                          });
                          await fireStore
                              .collection('clones')
                              .doc(cloneNameController.text)
                              .collection('chats')
                              .add({
                                'messageText':
                                    'Hello, I am your new clone ${cloneNameController.text}, Tap the switch on the top right corner of your screen to switch places with me.',
                                'whoSent': 'receiver',
                                'time': DateTime.now(),
                              })
                              .then((value) => Toast.show(
                                  'Clone added successfully',
                                  duration: Toast.lengthShort,
                                  backgroundColor: const Color(0xff903aff),
                                  gravity: Toast.center))
                              .catchError((error) => Toast.show(
                                  'Failed to add clone: $error',
                                  duration: Toast.lengthShort,
                                  backgroundColor: Colors.red,
                                  gravity: Toast.center));

                          cloneNameController.clear();
                          controller.reset();
                          Navigator.pop(context);
                        }
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
