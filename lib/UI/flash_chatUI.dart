import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flash_chat/Models/conversationListUi.dart';
import 'package:flash_chat/Models/constants.dart';
import 'package:flash_chat/UI/add-new-user-screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:progress_indicator_button/progress_button.dart';

class FlashChat extends StatefulWidget {
  const FlashChat({Key? key}) : super(key: key);

  @override
  State<FlashChat> createState() => _FlashChatState();
}

class _FlashChatState extends State<FlashChat> {
  Map<String, dynamic> data = {};

  static List<ConversationList> conversationList=[];

  List<ConversationList> displayList = List.from(conversationList);
  final fireStore = FirebaseFirestore.instance;
  late Stream<QuerySnapshot> cloneStream;
  final TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    cloneStream = fireStore.collection('clones').snapshots();
    searchController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context)  {


    Widget buildAlertDialog(BuildContext context) {
      return AlertDialog(
        title: const Text('FYI'),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
             const Text('Clones + Chats = Clats'),
             addVerticalSpacing(5),
            const Text('Tap and hold on a clone to delete it'),
          ],
        ),
        actions: [
          SizedBox(
              width: 75,
              height: 25,
              child: ProgressButton(
                color: Colors.transparent,
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                onPressed: (AnimationController controller) {
                  Navigator.pop(context);
                },
                child: const Text(
                  'Ok',
                  style: TextStyle(color: Colors.blue),
                ),
              )),
        ],
      );
    }

    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add_circle_outlined),
          onPressed: () {
            showModalBottomSheet(
                context: context,
                builder: (BuildContext context) => const AddNewUserScreen());
          }),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        flexibleSpace: Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(7),
                  bottomRight: Radius.circular(7)),
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomCenter,
                  colors: [Color(0xff903aff), Color(0xffdf44ff)])),
        ),
        centerTitle: true,
        elevation: 0,
        title: const Text('Clone-Chat'),
      ),
      body: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: EdgeInsets.all(5.0.r),
              child: Row(
                children: [
                  Text(
                    'Clats',
                    style: TextStyle(
                      fontSize: 24.sp,
                    ),
                  ),
                  addHorizontalSpacing(10),
                  GestureDetector(
                      onTap: () {
                        showDialog(context: context, builder: buildAlertDialog);
                      },
                      child: const Icon(Icons.info)),
                ],
              ),
            ),
          ),
          addVerticalSpacing(10),
          const Text('Tap the + button below to create a new ShadowClone'),
          addVerticalSpacing(10),
          Expanded(
            flex: 15,
            child: StreamBuilder<QuerySnapshot>(
                stream: cloneStream,
                builder: (context, snapshot) {

                  if (snapshot.hasError) {
                    return const Text('Something went wrong');
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return ListView(
                    children: snapshot.data!.docs.map((DocumentSnapshot document) {
                      Map<String, dynamic> data =
                      document.data()! as Map<String, dynamic>;
                      return ConversationList(data['cloneName']);
                    }).toList(),
                  );
                }),
          )
        ],
      ),
    );
  }
}
