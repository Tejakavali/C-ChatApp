import 'package:flash_chat/Models/chat_model.dart';
import 'package:flash_chat/Models/constants.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatScreen extends StatefulWidget {
  final String cloneName;
  const ChatScreen(this.cloneName, {Key? key}) : super(key: key);

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  late Stream<QuerySnapshot> chatStream;
  final fireStore = FirebaseFirestore.instance;
  final TextEditingController chatController = TextEditingController();

  @override
  void initState() {
    super.initState();
    chatStream = fireStore
        .collection('clones')
        .doc(widget.cloneName)
        .collection('chats')
        .orderBy('time', descending: false)
        .snapshots();
  }

  @override
  Widget build(BuildContext context) {
    // List<ChatMessagesModel> chatMessages = [];
    bool hasInternet;
    ToastContext().init(context);
    final chatProvider = Provider.of<ChatProvider>(context, listen: false);
    final chatProviderListen = Provider.of<ChatProvider>(context);
    String getUserStatus() {
      if (chatProviderListen.isClone) {
        return "receiver";
      } else {
        return "sender";
      }
    }

    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        backgroundColor: const Color(0xfff5f3f8),
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          flexibleSpace: Container(
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(7),
                    bottomRight: Radius.circular(7)),
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomCenter,
                    colors: [Color(0xff903aff), Color(0xffdf44ff)])),
          ),
          centerTitle: true,
          elevation: 0,
          title: Text(widget.cloneName),
          actions: [
            Switch.adaptive(
                value: chatProviderListen.isClone,
                onChanged: (value) {
                  chatProvider.toggleSwitch(value);
                }),
          ],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: StreamBuilder<QuerySnapshot>(
                  stream: chatStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something went wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return ListView(
                      children:
                          snapshot.data!.docs.map((DocumentSnapshot document) {
                        Map<String, dynamic> data =
                            document.data()! as Map<String, dynamic>;
                        return Align(
                          alignment: data['whoSent'] == "receiver"
                              ? Alignment.topLeft
                              : Alignment.topRight,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                                decoration: BoxDecoration(
                                    color: data['whoSent'] == "receiver"
                                        ?const Color(0xff903aff)
                                        :  const Color(0xffdf44ff),
                                    borderRadius: BorderRadius.circular(10)),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Text(data['messageText'],style:  kWhiteTextStyle,),
                                )),
                          ),
                        );
                      }).toList(),
                    );
                  }),
            ),
            Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Row(
                    children: [
                      addHorizontalSpacing(10),
                      Expanded(
                        flex: 4,
                        child: TextField(
                          controller: chatController,
                          textInputAction: TextInputAction.send,
                          cursorColor: Colors.black,
                          minLines: 1,
                          maxLines: 5,
                          keyboardType: TextInputType.multiline,
                          onChanged: (newValue) {},
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            focusColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.transparent,
                                  width: 0,
                                  style: BorderStyle.none),
                              borderRadius: BorderRadius.circular(25),
                            ),
                            suffixIcon: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GestureDetector(
                                  onTap: () async {
                                    hasInternet =
                                        await InternetConnectionChecker()
                                            .hasConnection;
                                    if (chatController.text.isEmpty) {
                                      Toast.show("Cannot send an empty message",
                                          duration: Toast.lengthShort,
                                          gravity: Toast.center,
                                          backgroundColor: Colors.red);
                                    } else if (hasInternet == false) {
                                      Toast.show("No internet connection",
                                          duration: Toast.lengthShort,
                                          gravity: Toast.center,
                                          backgroundColor: Colors.red);
                                    } else {
                                      await fireStore
                                          .collection('clones')
                                          .doc(widget.cloneName)
                                          .collection('chats')
                                          .add({
                                        'messageText': chatController.text,
                                        'whoSent': getUserStatus(),
                                        'time': DateTime.now(),
                                      });
                                      chatController.clear();
                                    }
                                  },
                                  child: const CircleAvatar(
                                      backgroundColor: Color(0xff3e2452),
                                      child: Icon(Icons.arrow_forward_ios))),
                            ),
                            border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  width: 0, style: BorderStyle.none),
                              borderRadius: BorderRadius.circular(25),
                            ),
                            hintText: 'Type Something...',
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 20.0),
                          ),
                        ),
                      ),
                      addHorizontalSpacing(10),
                    ],
                  ),
                ),
                addVerticalSpacing(10),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
